#!/bin/sh
set -e
#mkdir tp6
#cd tp6/

mkdir -p tmp root intermediate manc

# Environment variables (Test env ONLY !!):
export TP6_PWD_ROOT=toto
export TP6_PWD_INTER=titi
export TP6_PWD_MANC=tata
export HOME_PKI=$(pwd)


# Create the Root CA
cd root
echo -e "\nDirectory: $(pwd)"
rm -rf index.txt private certs crl
mkdir private certs crl
touch index.txt
echo 1000 > serial
echo 1000 > crlnumber

openssl genpkey -algorithm ec \
	-pkeyopt ec_paramgen_curve:secp521r1 \
	-pkeyopt ec_param_enc:named_curve \
	-aes128 -pass env:TP6_PWD_ROOT -out private/europole-root.key.pem

echo -e "Info Root Private key"
cat private/europole-root.key.pem | openssl pkey -text -noout -passin env:TP6_PWD_ROOT | grep -v "^\s\|p"

openssl req -batch -config openssl-root-cert.cnf -new -x509 -days 5000 \
	-key private/europole-root.key.pem -passin env:TP6_PWD_ROOT \
	-extensions v3_ca -out certs/europole-root.crt.pem

cat certs/europole-root.crt.pem | openssl x509 -text -noout
cd ..

# Create the Intermediate
cd intermediate
echo -e "\nDirectory: $(pwd)"
rm -rf index.txt private certs crl csr
mkdir csr private certs crl
touch index.txt
echo 1000 > serial
echo 1000 > crlnumber

openssl req -batch -config openssl-int-cert.cnf -new -newkey ec \
	-pkeyopt ec_paramgen_curve:secp384r1 \
	-pkeyopt ec_param_enc:named_curve \
	-passout env:TP6_PWD_INTER -keyout private/europole-int.key.pem \
	-out csr/europole-int.csr 

echo -e "Info Intermediate Private key"
cat private/europole-int.key.pem | openssl pkey -text -noout -passin env:TP6_PWD_INTER | grep -v "^\s\|p"

cp csr/europole-int.csr ../tmp/

# Back to the Root CA
cd ../root/
echo -e "\nDirectory: $(pwd)"

openssl ca -batch -config openssl-root-ca.cnf -passin env:TP6_PWD_ROOT \
	-extensions v3_inter_ca -in ../tmp/europole-int.csr \
	-notext -out ../tmp/europole-int.crt.pem > /dev/null 2>&1

cd ..

mv tmp/europole-int.crt.pem intermediate/certs/
rm tmp/*.*
cat intermediate/certs/europole-int.crt.pem | openssl x509 -text -noout

# Manc Orga
cd manc
echo -e "\nDirectory: $(pwd)"
rm -rf private certs csr
mkdir csr private certs 

openssl req -batch -config openssl-manc.cnf -new -newkey ec \
	-pkeyopt ec_paramgen_curve:secp384r1 \
	-pkeyopt ec_param_enc:named_curve \
	-passout env:TP6_PWD_MANC -keyout private/manc.key.pem \
	-out csr/manc.csr 

echo -e "Info MANC private key"
cat private/manc.key.pem | openssl pkey -text -noout -passin env:TP6_PWD_MANC | grep -v "^\s\|p"

cp csr/manc.csr ../tmp/

# Back to the intermediate CA
cd ../intermediate/
echo -e "\nDirectory: $(pwd)"

openssl ca -batch -config openssl-int-ca.cnf -passin env:TP6_PWD_INTER \
	-extensions server_cert -in ../tmp/manc.csr \
	-notext -out ../tmp/manc.crt.pem
#> /dev/null 2>&1

cd ..
mv tmp/manc.crt.pem manc/certs/
rm tmp/*.*

echo -e "\nInfo Manc Cert"
cat manc/certs/manc.crt.pem | openssl x509 -text -noout

# Construct the Certificate Chain (the order is important):
cat manc/certs/manc.crt.pem \
    intermediate/certs/europole-int.crt.pem \
    root/certs/europole-root.crt.pem \
    > manc/certs/manc-chain.crt.pem

# Time to check
echo -e "\n\n# To test, run both commands in different terminals:"
echo -e "openssl s_server -www -accept 4443 -state -cert manc/certs/manc.crt.pem -CAfile manc/certs/manc-chain.crt.pem -key manc/private/manc.key.pem -pass pass:$TP6_PWD_MANC"
echo -e "openssl s_client -connect localhost:4443 -CAfile root/certs/europole-root.crt.pem"

