
# Lab PKI

This demonstrate the use of PKI with 3 levels
- Root
- Intermediate
- a fictive "Manc Org."

I try to make is as simple as possible. It runs in Linux AND in Windows with "Git Bash".

- The Root is supposed to be offline. 
- The "Manc Org." is supposed to be an foreign entity using Subject Alternative Names (SAN).

## Tests

One way to test is to run the `openssl s_server` as indicated at the end of the script.
You should see `Verify return code: 0 (ok)` in the client.

## Subject Alternative Name SAN

The Common Name (CN) that used to carry the domain name (FQDN) of your web site, is now replaced by the Subject Alternative Name (SAN). But one issue is that the CN was part of the Subject, whether SAN are x509 extensions.  Requester Extensions used to be ignored and forced by the CA.   https://github.com/openssl/openssl/issues/3638 

In practice, CA use a Web form where you will type in the requested Subject Alternative Names. Same with the Key Usage and other extensions. They are requested by other means than the CSR.

## TODO

- put the CRL in another Branch.

